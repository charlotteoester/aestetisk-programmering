let vinkel = 0;

let speed1 = 0.8;
let speed2 = 1;
let speed3 = 0.6;
let speed4 = 0.9;
let speed5 = 0.45;
let speed6 = 0.7;
let speed7 = 0.3;
let speed8 = 0.5;
let speed9 = 0.85;

let vinkel1 = 0;
let vinkel2 = 0;
let vinkel3 = 0;
let vinkel4 = 0;
let vinkel5 = 0;
let vinkel6 = 0;
let vinkel7 = 0;
let vinkel8 = 0;
let vinkel9 = 0;

let img;
function preload() {
  img = loadImage('stjernehimlen.PNG');
}


function setup() {
  // put setup code here
  createCanvas(windowWidth, windowHeight);
    background(0);
    angleMode(DEGREES);
    
}



function draw() {
  // put drawing code here}


  background(0,300);
  image(img,0,0);
  translate(width/2, height/2); 
  noStroke();

  push();
 
  fill(255);
  rectMode(CENTER);
  
  
  //solen
    push();
  fill(255,255,0);
  ellipse(0,0,60,60);
  pop();
  //merkur
  push();
  rotate(vinkel1);
  vinkel1 = vinkel1 + speed1;
  fill(251,177,23);
  ellipse(35,35,20,20);
  pop();
  //venus
  push();
  rotate(vinkel2);
  vinkel2 = vinkel2 + speed2;
  fill(217,179,130);
  ellipse(58,58,30,30);
  push();
  fill(210,158,130);
  ellipse(58,58,3,30);
  ellipse(65,60,3,20);
  push();
  fill(250,198,173);
  ellipse(50,57,3,20);
  pop();
  pop();
  pop();
  //jorden
  push();
  rotate(vinkel3);
  vinkel3 = vinkel3 + speed3;
  fill(15,128,255);
  ellipse(85,85,35,35);
  push();
  fill(76,187,23);
  ellipse(88,93,8,10);
  ellipse(80,80,15,10);
  ellipse(100,86,5,12);
  pop();
  pop();
  //mars
  push();
  rotate(vinkel4);
  vinkel4 = vinkel4 + speed4;
  fill(255,86,0);
  ellipse(110,110,25,25);
  push();
  fill(255,50,0);
  ellipse(108,110,3,20);
  ellipse(115,117,3,5);
  ellipse(115,108,3,5);
  pop();
  pop();
  //jupter
  push();
  rotate(vinkel5);
  vinkel5 = vinkel5 + speed5;
  fill(232,210,190);
  ellipse(140,140,45,45);
  push();
  fill(195,155,119);
  ellipse(140,138,3,40);
  pop();
  push();
  fill(231,232,230);
  ellipse(148,143,3,30);
  pop();
  push();
  fill(200,120,10);
  ellipse(133,145,5,8);
  pop();
  pop();
  //saturn
  push();
  rotate(vinkel6);
  vinkel6 = vinkel6 + speed6;
  fill(207,185,151);
  ellipse(175,175,40,40);
  push();
  fill(197,159,110);
  ellipse(175,175,10,60);
  pop();
  pop();
  //uranus
  push();
  rotate(vinkel7);
  vinkel7 = vinkel7 + speed7;
  fill(0,96,255);
  ellipse(205,205,30,30);
  pop();
  //neptun
  push();
  rotate(vinkel8);
  vinkel8 = vinkel8 + speed8;
  fill(0,35,255);
  ellipse(230,230,25,25);
  push();
  fill(0,70,255);
  ellipse(230,228,5,10);
  ellipse(234,233,3,6);
  pop();
  pop();
  //pluto
  push();
  rotate(vinkel9);
  vinkel9 = vinkel9 + speed9;
  fill(175,128,79);
  ellipse(250,250,12,12);
  pop();

  pop();

  
  
}

  
 
