# MiniX 3
Min throbber skal forestille solsystemet med planeterne der roterer om solen.

Klik på linket for at se den bevæge sig:

[link](https://charlotteoester.gitlab.io/aestetisk-programmering/minix3/index.html)


## Idéen bag min throbber
Som nævnt har jeg taget udgangspunkt i solsystemet, hvilket jeg for det første har gjort for at udfordre den helt klassiske throbber man ser, hvor der blot er nogle prikker der roterer i en cirkel eller lignende - jeg tænkte at man lige så godt kunne få det til at ligne noget der eventuelt er relevant for hjemmesiden man er på, hvilket fører mig videre til min følgende pointe. For det andet ønskede jeg at gøre den mere legende, eftersom en throbber viser sig når computeren "tænker", og man samtidig er nødt til at vente et øjeblik. For hvorfor ik give brugeren af det digitale device noget mere spændende at kigge på mens de venter. Min bagtanke hertil er også, at tiden oftest - i hvert fald for de flestes vedkommende - synes at passere hurtigere når man er underholdt. Dermed mener jeg ikke der er nogen grund til, at en throbber skal være kedelig, for eksempel i form af nogle prikker der cirkulerer.

## Selve koden
Ved hjælp af funktionen `rotate();`, `rectMode(CENTER)` og `anglemode(DEGREES)` har jeg kreeret en form for loop, idet hele canvasset drejer omkring midten (solen) igen og igen. Dog har jeg benyttet mig af forskellige variabler i form af `vinkel` og `speed` for at få planeterne til at dreje om solen med forskellige hastigheder, og dermed give illusionen af det virkelige solsystem.
Jeg kunne også have fået dem til at starte forskellige steder ved at ændre på min variabel "vinkel", men endte med bedre at kunne lide den måde de starter samme sted, og spreder sig ud forskelligt ved hjælp af min variabel "speed". På den måde ender de også med at samle sig på linje igen efter noget tid, hvilket er med til at skabe dette loop. Derudover legede jeg også med idéen om at få dem til at køre i en hastighed svarende til forholdet mellem de virkelige planeter i solsystemet og deres rotation om solen. Dog var dette en lidt for stor mundfuld i forhold til det tidspres jeg var under (selvforskyldt, ups).

En ny funktion/syntax jeg har benyttet mig af denne gang, er at have et billed som baggrund for at illustrere rummet og stjernehimlen omkring solsystemet. Funktionen jeg har benyttet til dette er `function preload()`.

## Reflektion om throbbers
Throbbers er noget man tit støder på, men egentlig ikke noget der har strejfet mig en synderligt stor tanke før nu. Når jeg tænker på en throbber er det eneste jeg tænker på nogle prikker eller streger der kører i ring. Men når nu hjemmesider og så videre har gjort noget ud af at gøre en throbber anderldes eller humoristisk, er det også noget der har indflydelse på brugeroplevelsen af hjemmesiden. Det giver en følelse af gennemførthed og detaljegraden er noget jeg selv sætter pris på. En throbber kan kommunikere et hav af forskellige ting, og det er op til hjemmesiden og designeren at formidle det de ønsker med den. På den måde har den som så mange andre ting en form for magt, eftersom det er noget brugerne ufrivilligt bliver 'udsat' for.






