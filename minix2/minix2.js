function setup() {
  // put setup code here
  createCanvas(1000,500);
  background(90,90,200);
}

function draw() {
  // put drawing code here}
 //Ansigt
 noStroke();
 push();
 fill(228, 185, 142);
 ellipse(250, 250, 215, 260);

 ellipse(140, 230, 27, 40);
 ellipse(360, 230, 27, 40);
 pop();

 //Mund
 push();
 noStroke();
 fill(0);
 arc(250, 290, 80, 80, 0, PI, CHORD);
 pop();

 //Næse
 push();
 fill(300, 205, 150);
 beginShape();
 vertex(250, 230);
 vertex(245, 230);
 vertex(235, 260);
 vertex(250, 270);
 endShape(CLOSE);
 pop();

 //Øjne
 push();
 fill(0);
 ellipse(200, 220, 27, 35);
 ellipse(300, 220, 27, 35);
 pop();

 push();
 fill(255);
 ellipse(195, 215, 10, 15);
 ellipse(295, 215, 10, 15);
 pop();

 //Hår
if (typeof(lMouseOver2) === "undefined") {
   lR2 = 106;
  lG2 = 78;
  lB2 = 66;
 lMouseOver2 = false;
}
lMouseOverWas2 = lMouseOver2;
lMouseOver2 = false;
if ((mouseX > 160) && (mouseX < 340)
  && (mouseY > 128) && (mouseY < 180))
 {
  lMouseOver2 = true;
  if (lMouseOver2 != lMouseOverWas2) {
    lR2 = random(0,255);
    lG2 = random(0,255);
    lB2 = random(0,255);
  }
 }
 else {
  lR2 = 106;
  lG2 = 78;
  lB2 = 66;

 }

push();
fill(lR2,lG2,lB2);

circle(305,150,70);
circle(250,140,70);
circle(195,150,70);
circle(160,180,40);
circle(340,180,40);
circle(285,128,45);
circle(220,128,45);
circle(168,155,35);
circle(335,155,35)
pop();
 
//Bumser
push();
fill(255,120,110);
circle(280,190,8)
circle(175,270,10)
circle(165,255,5)
circle(285,345,7)
circle(325,310,9)
pop();

push();
fill(255)
circle(279,189,3)
circle(174,269,3)
circle(164,254,2)
circle(284,344,3)
circle(324,309,3)
pop();

push();
fill(210,145,100);
circle(315,295,6)
circle(265,195,4)
pop();



 //-------------------------------------------



  //Ny emoji
  noStroke();
  //Ansigt
  push();
  fill(228, 185, 142);
  ellipse(700, 250, 215, 260);

  ellipse(590, 230, 27, 40);
  ellipse(810, 230, 27, 40);
  pop();

  //Mund
  push();
  noStroke();
  fill(0);
  arc(700, 290, 80, 80, 0, PI, CHORD);
  pop();

  //Næse
  push();
  fill(300, 205, 150);
  beginShape();
  vertex(700, 230);
  vertex(695, 230);
  vertex(685, 260);
  vertex(700, 270);
  endShape(CLOSE);
  pop();

  //Øjne
  push();
  fill(0);
  ellipse(650, 220, 27, 35);
  ellipse(750, 220, 27, 35);
  pop();

  push();
  fill(255);
  ellipse(645, 215, 10, 15);
  ellipse(745, 215, 10, 15);
  pop();

  //Fregner
  push();
  fill(106,78,66);
  circle(665,250,5);
  circle(645,250,5);
  circle(655,263,5);

  circle(735,250,5);
  circle(755,250,5);
  circle(745,263,5);
  pop();

//Hår
if (typeof(lMouseOver) === "undefined") {
  lMouseOver = false;
  lR = 106;
  lG = 78;
  lB = 66;
}
lMouseOverWas = lMouseOver;
lMouseOver = false;
if ((mouseX > 610) && (mouseX < 790)
  && (mouseY > 128) && (mouseY < 180))
 {
  lMouseOver = true;
  if (lMouseOver != lMouseOverWas) {
    lR = random(0,255);
    lG = random(0,255);
    lB = random(0,255);
  }
 }
 else {
  lR = 106;
  lG = 78;
  lB = 66;
}

  push();
fill(lR,lG,lB);

  circle(755,150,70);
  circle(700,140,70);
  circle(645,150,70);
  circle(610,180,40);
  circle(790,180,40);
  circle(735,128,45);
  circle(670,128,45);
  circle(618,155,35);
  circle(785,155,35)
  pop();
  

 


}

  
 
