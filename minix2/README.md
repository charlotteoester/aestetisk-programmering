# MiniX 2

Mine emojis skal forestille to personer, en med fregner og en med akne, som man kan se på billedet herunder.

![](https://charlotteoester.gitlab.io/aestetisk-programmering/MiniX2/Screenshotminix2.PNG)

Linket til mine emojis kan findes her:
https://charlotteoester.gitlab.io/aestetisk-programmering/MiniX2/index.html

## Beskrivelse af mit program

Selve konstruktionen af ansigtet er opbygget meget simpelt af forskellige geometriske figurer - mest ellipser - dog har jeg som noget nyt benyttet mig af `beginShape();` `endShape(CLOSE);` og derimellem `vertex();` for at skabe min egen figur, i dette tilfælde i form af næsen på mine emojis. Jeg har også benyttet mig af `arc();` for at lave munden, hvilket også er nyt for mig. Den helt store udfordring ved udformningen af disse var dog, at jeg ville give mig i kast med, at få håret til at skifte til en tilfældig farve, når man holder musen henover håret. Dertil ville jeg have at det skulle være en ny farve hver gang man kørte musen henover håret. For at kunne opnå dette, har jeg benyttet mig af nogle if-else statements samt `mouseX` og `mouseY` for at afgrænse området musen skal være i, for at mit statement er true. Dette gjorde jeg ved at sige at mouseX skulle være mindre end`<` og større end`>` nogle givne værdier, og det samme gælder for mouseY.
Jeg ville dermed gerne have, at håret er en `random();` farve "if" musen er indenfor det afgrænsede område, og "else" skulle det være den brunlige farve. Idéelt set ville jeg også gerne have haft at hudfarven kunne skifte farve, og legede med idéen om forskellige øjenfarver, dog nåede jeg ikke i mål med dette denne gang.
Derudover har jeg leget rundt med at benytte mig af forskellige variabler. For at undgå at min kode stopper med en fejl første gang jeg bruger variablen `mouseover`, skulle jeg sikre mig at den findes. Dette tjekkede jeg ved at undersøge hvilken type variablen er `typeof()`, om det er tal, tekst eller boolean. Hvis den så ikke findes er typen `undefined`, og dermed kan man sige: hvis typeof mouseover er undefined, så må jeg hellere lige definere den for at undgå koden stopper med fejl. Man definerer den ved at tildele`=` den en værdi, eller hvis det er en boolean, så tildeles `false`. Jeg beregner altså en `boolean`, som hedder mouseover, som bliver `true` hvis musen er indenfor det afgrænsede område. Men eftersom jeg kun vil skifte farve når jeg flytter musen indenfor området, så vil jeg egentlig vide om jeg skifter fra false til true. Derfor gemmer jeg hvad mouseover lige har været `mouseoverwas`, så jeg ved om den var false og og om den nu bliver til true idet musen kommer ind i området. Så hvis den er true og før var false, så skal den finde en ny random farve. Hvis ikke, ville den skifte farve hele tiden mens musen er i feltet.

Selve afgrænsningen af området er ikke helt præcist indenfor hårets form, da det var en stor mundfuld at regne ud hvordan det skulle fungere pga. dets meget organiske form - dog vil jeg sige det er lykkedes nogenlunde fyldestgørende alligevel.

## Den dybere mening

Grunden til at jeg har valgt at mine emojis skulle forestille to personer med henholdsvis akne og fregner er, at jeg har spekuleret over, hvor virkelighedstro emojis behøver at være, og hvor den grænse går. Der er og har været en del snak om at alle skal være repræsenteret når det kommer til emojis, men når man tænker således over det, er der ingen grænser for hvor mange detaljer man kan dykke ned i, hvis alle skal føle sig 100% inkluderede. Det er blandt andet en stor debat i forhold til hudfarve, hvilket førte mig videre til tanken om, at andre hudtilstande såsom akne og fregner endnu ikke er repræsenterede. Dertil er det også kun de naturlige hårfarver vi ser på de nuværende emojis, og hvis vi går så meget op i, at der skal være en der ligner/repræsenterer hver enkelt person, hvorfor så ikke lave en emoji med f.eks. blåt eller grønt hår. Derfor legede jeg i koden med, at få håret til at skifte til en tilfældig farve ud af alle de mulige farvekoder der findes.

Hertil kan man så snakke om, at hvis man implementerede flere hudtilstande og hårfarver i alle regnbuens farver, vil dem der så ikke føler sig repræsenterede pludselig føle sig endnu mere ekskluderede. Derfor vil det ingen ende få, og der vil altid være flere detaljer at tilføje. dermed mener jeg også det er utroligt svært at finde den rigtige balance mellem for generaliserende og for detaljeret når det kommer til emojis.

[Sourcecode] (https://gitlab.com/charlotteoester/aestetisk-programmering/-/blob/main/MiniX2/minix2.js)





