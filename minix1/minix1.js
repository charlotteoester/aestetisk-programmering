function setup() {
  // put setup code here
  createCanvas(500,500);
  background(110,180,255);
}

function draw() {
  // put drawing code here
  //Kroppen
  noStroke();
  ellipse(250,430,200);
  ellipse(250,300,160);
  ellipse(250,185,125);

  //Gulerodsnæse
  push();
  fill(255, 140, 0);
  triangle(250, 175, 250, 195, 160, 185);
  pop();

  //Øjne og mund
  push();
  fill( 0);
  ellipse(220,160,12);
  ellipse(270,160,12);
  ellipse(270,160,12);
  ellipse(280,205,10);
  ellipse(220,205,10);
  ellipse(250,220,10);
  ellipse(233,215,10);
  ellipse(267,215,10);
  pop();

  //Arme
  push();
  fill(100,69,19);
  rect(320, 285, 115, 10);
  rect(65, 285, 115, 10);
  pop();

  //Knapper
  push();
  fill( 0);
  ellipse(250,265,17);
  ellipse(250,295,17);
  ellipse(250,325,17);
  pop();

  //Hat
  push();
  fill( 0);
  ellipse(250,130,150,25);
  rect(212, 55, 80, 80);
  ellipse(252,56,80,25);
  pop();

  //Halstørklæde
  push();
  fill(255,0,0);
  ellipse(250,235,110,15);
  ellipse(280,275,20,90);
  ellipse(275,320,3,15);
  ellipse(280,320,3,15);
  ellipse(285,320,3,15);
  pop();

  //Sne
  circle(30, 25, 10);
  circle(90, 35, 10);
  circle(150, 25, 10);
  circle(210, 35, 10);
  circle(270,25, 10);
  circle(330, 35, 10);
  circle(390, 25, 10);
  circle(450, 35, 10);
  
  circle(50, 75, 8);
  circle(110, 85, 8);
  circle(170, 75, 8);
  circle(350, 85, 8);
  circle(410, 75, 8);
  circle(470, 85, 8);

  circle(30, 125, 10);
  circle(90, 135, 10);
  circle(150, 125, 10);
  circle(330, 135, 10);
  circle(390, 125, 10);
  circle(450, 135, 10);

  circle(50, 175, 8);
  circle(110, 185, 8);
  circle(170, 175, 8);
  circle(350, 185, 8);
  circle(410, 175, 8);
  circle(470, 185, 8);

  circle(30, 225, 10);
  circle(90, 235, 10);
  circle(150, 225, 10);
  circle(330, 235, 10);
  circle(390, 225, 10);
  circle(450, 235, 10);

  circle(50, 275, 8);
  circle(110, 285, 8);
  circle(170, 275, 8);
  circle(350, 285, 8);
  circle(410, 275, 8);
  circle(470, 285, 8);

  circle(30, 325, 10);
  circle(90, 335, 10);
  circle(150, 325, 10);
  circle(330, 335, 10);
  circle(390, 325, 10);
  circle(450, 335, 10);

  circle(50, 375, 8);
  circle(110, 385, 8);
  circle(170, 375, 8);
  circle(230, 385, 8);
  circle(290, 375, 8);
  circle(350, 385, 8);
  circle(410, 375, 8);
  circle(470, 385, 8);

  circle(30, 425, 10);
  circle(90, 435, 10);
  circle(150, 425, 10);
  circle(210, 435, 10);
  circle(270,425, 10);
  circle(330, 435, 10);
  circle(390, 425, 10);
  circle(450, 435, 10);

  circle(50, 475, 8);
  circle(110, 485, 8);
  circle(170, 475, 8);
  circle(230, 485, 8);
  circle(290, 475, 8);
  circle(350, 485, 8);
  circle(410, 475, 8);
  circle(470, 485, 8);
}
